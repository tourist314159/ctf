from pwn import *

r = remote("crypto.chal.csaw.io", 8040)

r.recvuntil("Encrypting service")



#     AAAAAAAAAAAAAAAABBBBBBBBBBBBBBBCCCCCCCCCCCCCCCC
#AES("CSAW{BLAGBLAH}MYSTUFFHEREMYSTUFFHEREXXXXXXXXXXX"

r.sendline("\x00"*50)
ct = (r.recvuntil("Encrypting service"))

for i in range(0, len(ct), 16):
	print(" ".join([hex(ord(k)+256)[-2:] for k in ct[i:i+16]]))
