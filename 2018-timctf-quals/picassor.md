[Go back](.)

### Picassor - 50 points

The given image wasn't recognized by file nor binwalk so I guessed it had to be encrypted. Pronouncing the name might suggest an XOR cipher so I implemented one and brute-forced all the possible 1-byte keys. The correct key turned out to be 171 and I got this image containing the flag:

![timctf{x0r_and_rul3_un1t3_and_l34d](./picassor/171.jpg)

