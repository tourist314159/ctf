[Go back](.)

### Those are rookie numbers - 30 points

The task provides an RSA public key (n, e) and a message (c) encrypted with it:

```py
n = 58900433780152059829684181006276669633073820320761216330291745734792546625247
e = 65537
c = 56191946659070299323432594589209132754159316947267240359739328886944131258862
```

One of the strenghts of the RSA cryptosystem lies in the difficulty of factoring n, but in this case a quick lookup on [factordb](//factordb.com) helped us find the two prime factors of n:

```py
p = 176773485669509339371361332756951225661
q = 333197218785800427026869958933009188427

assert(n == p * q)
```

Knowing p and q we can easily calculate phi:

```py
phi = (p - 1) * (q - 1)
```

And then the private key, d, using any modular inverse implementation:

```py
d = modular_inverse(e, phi)
```

Now that we know d we can decrypt the message by raising c to the d'th power modulo n and turning that number into readable ascii:

```py
import binascii

m = pow(c, d, n)

print binascii.unhexlify(hex(m)[2:-1]) # => timctf{th0sE_rOoKIe_numB3rz}
```
