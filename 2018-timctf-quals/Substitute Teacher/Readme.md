[Go back](..)

### Substitute Teacher - 100 points

The given python script implements a substitution cipher that only affects lowercase letters. This means that line 8 of the ciphertext (which is all uppercase) remained unchanged. Searching for "FOR BLYTHE... AGAIN. MORE THAN EVER." got many results about the book "The Da Vinci Code" by Dan Brown. This made it evident that "Tob Dk Vinfi Cexb" in the ciphertext corresponded to the title of the book.

```py
match("Tob Dk Vinfi Cexb", "The Da Vinci Code")
```

One way I could've solved this is by actually finding the book contents and manually matching them with the ciphertext but instead of that I plugged the first paragraph into [quipqiup](//quipqiup.com) and got that the first few words are "First and foremost, to my friend and editor".

```py
match("Figsc knx hegbmesc, ce mr hgibnx knx bxiceg",
      "First and foremost, to my friend and editor")
```

These few words covered a significant part of the english alphabet, thus revealing most of the contents of the text. To finish the task all that was needed to do is swap a few lesser-used letters (z, j, q etc.) around until all words that they appeared in decrypted correctly.

```py
match("zgeybfc", "project")
match("hdttr bjzgbss mr agkcicdxb",
	"fully express my gratitude")
match("ueeq", "book")
match("nepbt", "novel")
match("1234567890", "1234567890")
match("Biutiecobwdb", "Bibliotheque")
```

The `match` function was defined as follows:

```py
coresp = {}

def match(encrypted_string, decrypted_string):
	assert(len(encrypted_string) == len(decrypted_string))
	for i in range(len(encrypted_string)):
		coresp[encrypted_string[i]] = decrypted_string[i]

```

All that was needed to do now is write the decrypted text to a file:

```py
solved = ""
for c in ciphertext:
	if c in coresp:
		solved = solved + coresp[c]
	else:
		solved = solved + c

open("solved.txt", "w").write(solved)
```

A simple search through that file for `timctf` revealed the flag: `timctf{fr3quencyan4lyS1s1sc0ol}`
