[Go back](..)

### SSS Part 2 - 250 points

#### Shamir Secret Sharing primer

The Shamir Secret Sharing scheme is a way of transforming a secret (encoded as a number) into a series of points represented by coordinate pairs. The goal is that one shouldn't be able to recover the secret without knowing at least a certain number of the points.

In order to split a secret number S in such a way that you'd need at least k points to recover it you have to first create a polynomial f(x) in such a manner that f(0) is your secret AND the polynomial's degree is k-1. After creating that polynomial choose various values for x and compute f(x). Each (x, f(x)) pair is a share that you can distribute.

The main idea of the scheme is that in order to correctly define a degree n polynomial one needs n+1 points. For example, you need 2 points to define a line (a degree 1 polynomial), 3 points to define a parabola (a degree 2 polynomial) etc. Knowing less than that leads to an infinite number of potential solutions.

Once you know k points you can use [Lagrange Polynomial Interpolation](//en.wikipedia.org/wiki/Lagrange_polynomial) to go beyond the known points and compute the polynomial for any x value, including 0, thus recovering the secret.

#### The task

The sss.py file contains some code that generates such secret splitters. split.py begins by generating a scheme with 16 pieces that requires at least 13 of them to calculate the secret. After that it splits the flag into 10 character chunks and splits each chunk into a bunch of points, but only reveals 10 of them. As we require 13 points to get the chunks back we aren't able to do so using the "normal" method.

The huge weakness of this scheme is that the same polynomial coefficients are reused for all the chunks. For example, the polynomial of chunk 0 looks like:

```math
f_0(x) = \text{chunk}_0 + c_1*x + c_2*x^2 + c_3*x^3 + \ldots + c_{12}*x^{12}
```

And the polynomial of chunk 1 looks like:

```math
f_1(x) = \text{chunk}_1 + c_1*x + c_2*x^2 + c_3*x^3 + \ldots + c_{12}*x^{12}
```

With the same $`c_i`$ values (and that's the big nono). Thus if we know the values of $`f_0`$ and $`f_1`$ for the same x we can calculate $`\text{chunk}_0 - \text{chunk}_1`$:

```math
\begin{aligned}
f_0(x) - f_1(x) =\  &\text{chunk}_0 + c_1*x + c_2*x^2 + c_3*x^3 + \ldots + c_{12}*x^{12} - \\
&\text{chunk}_1 - c_1*x - c_2*x^2 - c_3*x^3 - \ldots - c_{12}*x^{12} = \\
=\ &\text{chunk}_0 - \text{chunk}_1
\end{aligned}
```

There are two useful assumptions (that will turn out to be true) we can make:

#### Assumption 1
The last chunk (chunk 3) ends in a closing bracket = ASCII 125

Let $`a`$ represent the last 8 bytes = last character of chunk 0. Considering the assumption is true, then:
```math
\begin{aligned}
&\text{chunk}_0 \equiv a \text{ mod } 256\\
&\text{chunk}_3 \equiv 125 \text{ mod } 256\\
\implies &\text{chunk}_0 - \text{chunk}_3 \equiv a - 125 \text{ mod } 256 \quad \text{(1)}
\end{aligned}
```

We know the polynomial value for both of these chunks at x = 11:

```math
\begin{aligned}
&\text{chunk}_0 - \text{chunk}_3 = f_0(11) - f_3(11)\\
&\text{chunk}_0 - \text{chunk}_3 = 678799734583786557149135603414899824 - 678799734583236817869360194048995726 \quad \text{(2)}
\end{aligned}
```
```math
\begin{aligned}
(1), (2) \implies &a - 125 \equiv 549739279775409365904098 \text{ mod } 256\\
&a - 125 \equiv 226 \text{ mod } 256\\
&a = 95
\end{aligned}
```

Then the last character of chunk 0 is an underscore (ASCII 95).

#### Assumption 2
Chunk 0 looks like `timctf{???`

What's left now is to go through all possible valid character combinations for the last 3 characters of the first chunk. This can be greatly sped up using the results we got earlier, as we know what the last character is. We now only need to search through all possible character pairs.

#### Solution

The differences in between chunk0 and the other chunks are precomputed and then all possible values for chunk0 are tried. For each possible chunk0 we add back the differences to get the other chunks and then check if the flag obtained by concatenating them would be valid. All potential flags are written to a file for manual examination.

```py
import binascii

valid_flag_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

# chunk0 and chunk1 have known y values for x = 1
chunk0_minus_chunk1 = 7714685804569579757659784L - 7534756766114272409848924L

# x = 2
chunk0_minus_chunk2 = 3547087743966283680068135913L - 3547059021367945143469206505L

# x = 1
chunk0_minus_chunk3 = 7714685804569579757659784L - 7164946524794170391755686L

solution_file = open("solutions.txt", "w")

for c1 in valid_flag_chars:
	for c2 in valid_flag_chars:
		chunk0 = "timctf{" + c1 + c2 + "_"

		chunk0_int = int(chunk0.encode("hex"), 16)

		chunk1_int = chunk0_int - chunk0_minus_chunk1
		chunk2_int = chunk0_int - chunk0_minus_chunk2
		chunk3_int = chunk0_int - chunk0_minus_chunk3

		chunk1 = binascii.unhexlify(hex(chunk1_int)[2:-1])
		chunk2 = binascii.unhexlify(hex(chunk2_int)[2:-1])
		chunk3 = binascii.unhexlify(hex(chunk3_int)[2:-1])

		potential_flag = chunk0 + chunk1 + chunk2 + chunk3

		valid = True
		for c in potential_flag[7:-1]:	# ignore `timctf{` and `}`
			if c not in valid_flag_chars:
				valid = False

		if valid:
			solution_file.write(potential_flag + "\n")

solution_file.close()

```

There's quite a bunch of flags in the solutions.txt file and I think they could be more thoroughly verified using all the information we are given about the polynomials, not just the few points that we used, but the manual selection process was quite quick and the correct flag was `timctf{d0_NOt_R3inV3nT_CrYpt0_Pl34sE}`
