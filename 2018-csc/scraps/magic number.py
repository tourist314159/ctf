
def find():
	digits = "9876543210"

	for a in digits:
		for b in digits:
			if b == a:
				continue
			for c in digits:
				if c == b or c == a:
					continue
				for d in digits:
					if d == c or d == b or d == a:
						continue
					for e in digits:
						if e == d or e == c or e == b or e == a:
							continue
						for f in digits:
							if f == e or f == d or f == c or f == b or f == a:
								continue
							for g in digits:
								if g == f or g == e or g == d or g == c or g == b or g == a:
									continue
								for h in digits:
									if h == g or h == f or h == e or h == d or h == c or h == b or h == a:
										continue
									for i in digits:
										if i == h or i == g or i == f or i == e or i == d or i == c or i == b or i == a:
											continue
										for j in digits:
											if j == h or j == g or j == f or j == e or j == d or j == c or j == b or j == a:
												continue
											n = a+b+c+d+e+f+g+h+i+j
											ok=1
											for p in range(9):
												if str(int(n[p]) * int(n[p+1])) not in n:
													ok=0
											if ok==1:
												return n


password = find()

# 9872305614
# echo U2FsdGVkX1/9Mn5FmMWRvC7FYywB005oHQNClUQZ7pmUT2W7SqE2I00vhmGK/gZx | base64 -d | openssl aes-128-cbc -d -k 9872305614
# 44▒25'42.0"N 26▒03'30.4"E
# ECSC{44 25 42.0N 26 03 30.4E}