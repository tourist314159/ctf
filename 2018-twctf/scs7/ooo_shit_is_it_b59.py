from pwn import *

r = remote("crypto.chal.ctf.westerns.tokyo", 14791)
requestcount = 0
r.recvuntil("encrypted flag: ")
encflag = r.recvline()[:-1]
print("Target flag: \n" + encflag)

r.recvuntil("message: ")
r.sendline(chr(1))
r.recvuntil("ciphertext: ")
print 1, r.recvline()[:-1]
for i in range(58, 120):
	r.recvuntil("message: ")
	r.sendline(chr(i))
	r.recvuntil("ciphertext: ")
	print i, r.recvline()[:-1]
