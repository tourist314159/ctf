# TWCTF 2018

## Misc

### Welcome!! (29 points - might change beore the end)

The flag is in the task description: `TWCTF{Welcome_TokyoWesterns_CTF_2018!!}`

### [Mondai (98 points - might change before the end)](./mondai/Readme.md)

## Web

### [SimpleAuth (57 points - might change before the end)](./simpleauth/Readme.md)
### [Shrine (206 points - might change before the end)](./shrine/Readme.md)
### [Slack emoji converter (284 points - might change before the end)](./emoji/Readme.md)

## Cryptography / Reverse

### [dec dec dec (104 points - might change before the end)](./dec/Readme.md)
### [scs7 (117 points - might change before the end)](./scs7/Readme.md)
