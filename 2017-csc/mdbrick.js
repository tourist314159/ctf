/*

# Bricks were MD* - 100 points

## Solution
This is using one of the hash pairs found in the Xiaoyun Wang paper[1].
First I manually copied and reversed the endianness of each to-be-hashed
hex string. The 2 for loops convert the 256-long hex strings to 128-long
messages to be piped over to netcat.


[1] http://eprint.iacr.org/2004/199.pdf

*/

const a_hex = "d131dd02c5e6eec4693d9a0698aff95c2fcab58712467eab4004583eb8fb7f8955ad340609f4b30283e488832571415a085125e8f7cdc99fd91dbdf280373c5bd8823e3156348f5bae6dacd436c919c6dd53e2b487da03fd02396306d248cda0e99f33420f577ee8ce54b67080a80d1ec69821bcb6a8839396f9652b6ff72a70"
const b_hex = "d131dd02c5e6eec4693d9a0698aff95c2fcab50712467eab4004583eb8fb7f8955ad340609f4b30283e4888325f1415a085125e8f7cdc99fd91dbd7280373c5bd8823e3156348f5bae6dacd436c919c6dd53e23487da03fd02396306d248cda0e99f33420f577ee8ce54b67080280d1ec69821bcb6a8839396f965ab6ff72a70"

var a_msg = "";
for(var i=0; i<a_hex.length; i+=2)
    a_msg+=String.fromCharCode(parseInt(a_hex[i] + a_hex[i+1], 16))

var b_msg = "";
for(var i=0; i<b_hex.length; i+=2)
    b_msg+=String.fromCharCode(parseInt(b_hex[i] + b_hex[i+1], 16))


console.log(a_msg)
console.log(b_msg)
