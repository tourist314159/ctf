/*

# The Tiny Horror - 100 points

## Part II - creating the correspondence array

I went through all 8bit values, writing each to `one_char_at_a_time` and
running `./tiny_horror one_char_at_a_time` to get its "encrypted" value.
`which_is_which.c` outputs an almost syntactically correct C array declaration
of the `dec_to_enc` array used in `decode.c`

*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    int i;
    printf("const unsigned char dec_to_enc[] = {");
    for(i=0; i<256; i++) {
        FILE *f = fopen("one_char_at_a_time", "w");
        fprintf(f, "%c", i);
        fclose(f);
        system("./tiny_horror one_char_at_a_time");
        f = fopen("one_char_at_a_time.encrypted", "r");

        unsigned char c;
        fscanf(f, "%c", &c);
        fclose(f);
        printf("%d, ", c);
    }
    printf("};\n");

    return 0;
}
