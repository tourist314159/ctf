# The Tiny Horror - 100 points

## Part I - figuring the algorithm out

By running `./tiny_horror` on some dummy files I noticed that the input size is
equal to the output size. That and the fact that repeated chars turn into
strings of zeroes lead me to think that each input byte corresponds to exactly
one output byte which is either xor-ed or subtracted from the previous. Now I
set up a jankyer version of `which_is_which.c` to test the xor hypothesis,
encrypted "deadbeef" using `./tiny_horror` and decrypted it on paper:

![Pen'n'paper decryption photo](./pen_n_paper.jpg)

(ignore the strikethrough line - hexdump swapped every odd-index byte with
the next one and I thought at first that was the encryptor's doing)

That helped me deduce the algorithm:
```
encrypted[0] = lookup(input[0])
encrypted[i] = encrypted[i-1] xor lookup(input[i]) for i>0
```
and reverse it:
```
decrypted[0] = encrypted[0]
decrypted[i] = decrypted[i-1] xor encrypted[i] for i>0
apply inverse of lookup() to each decrypted[] element
```

## Part II - creating the correspondence array

I went through all 8bit values, writing each to `one_char_at_a_time` and
running `./tiny_horror one_char_at_a_time` to get its "encrypted" value.
`which_is_which.c` outputs an almost syntactically correct C array declaration
of the `dec_to_enc` array used in `decode.c`

## Part III - decrypting

`decode.c` first generates `enc_to_dec[]` from the previously made
`dec_to_enc[]`. After that it reads the encrypted file byte by byte, xor-ing
the accumulator `last_decoded` with the read value at each step and writing the
corresponding char `enc_to_dec[last_decoded]` to stdout.

# Running

```bash
gcc which_is_which.c
./a.out
# Copy the output to the beginning of decode.c and erase the last comma
gcc decode.c
./a.out mystery.encrypted > flag.png
```
