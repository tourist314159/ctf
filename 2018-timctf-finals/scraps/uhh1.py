from pwn import *

r = remote("89.38.210.128", 337)

# Gather 11 points
x = []
y = []
for i in range(11):
	r.recvuntil("Command: ")
	r.sendline("1")
	r.recvuntil("message:")
	r.sendline(hex(i)[2:])
	r.recvuntil(">>>> ")
	x += [i * 256 + 255]
	y += [int(r.recvline()[2:], 16)]

print x, y

# Solve 11 encrypion challenges
r.recvuntil("Command: ")
r.sendline("2")
for _ in range(11):
	r.recvuntil("Encrypt this: 0x")
	chal = r.recvline()
	print chal
	X = int(chal, 16) * 256 + 255

	# Lagrange interpolation
	res = 0
	for j in range(11):
		lX_fraction_top = 1L
		lX_fraction_bottom = 1L
		for m in range(11):
			if m == j:
				continue
			lX_fraction_top *= (X - x[m])
			lX_fraction_bottom *= (x[j] - x[m])
		assert(lX_fraction_top % lX_fraction_bottom == 0)
		res += y[j] * lX_fraction_top // lX_fraction_bottom

	res = res % (11 ** 128)

	# Send the solution
	solution = hex(res)[2:]
	print "Sending: " + solution
	r.sendline(solution)

r.interactive()
