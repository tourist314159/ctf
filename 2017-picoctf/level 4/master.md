[Go back](./Readme.md)

## Master Challenge - weirderRSA - 175 points

Given e, n, dp and an encrypted message, I should decrypt it.

Starting with the definition of $`d_p`$ from the RSA Chinese Remainder Theorem optimisation:

$`d_p = d \text{ mod } (p-1)`$

(1) $`d_p < p - 1`$

From the RSA theory:

$`e*d = 1 \text{ mod } (p-1)(q-1)`$

$`e*d = 1 + k(p-1)(q-1), \exists k`$

$`e*d = 1 + l(p-1), \text{ where } l = k(q-1)`$

$`e*d = 1 \text{ mod } (p-1)`$

$`(e \text{ mod } (p-1))(d \text{ mod } (p-1)) = 1 \text{ mod } (p-1)`$

$`e*d_p = 1 \text{ mod } (p-1)`$

(2) $`e*d_p = 1 + m(p-1), \exists m`$

From (1) and (2) it can be concluded that:

$`e > m`$

Since e is a relatively small number, all the possible values for m can be tried to generate p:

(2) $`\Rightarrow p = 1 + \frac{e*d_p - 1}{m}`$

$`q = \frac{n}{p}`$

$`d = e^{-1} \text{ mod } (p-1)(q-1)`$

$`\text{flag} = c^d \text{ mod } n`$

This python script applies this algorithm:

```py
from binascii import unhexlify

def extended_gcd(aa, bb):
    lastremainder, remainder = abs(aa), abs(bb)
    x, lastx, y, lasty = 0, 1, 1, 0
    while remainder:
        lastremainder, (quotient, remainder) = remainder, divmod(lastremainder, remainder)
        x, lastx = lastx - quotient*x, x
        y, lasty = lasty - quotient*y, y
    return lastremainder, lastx * (-1 if aa < 0 else 1), lasty * (-1 if bb < 0 else 1)
 
def modinv(a, m):
    g, x, y = extended_gcd(a, m)
    if g != 1:
        raise ValueError
    return x % m

e = 65537
n = 499607129142614016115845569972309954865466642986629586633467546172800056547903083303297314393486719922392114168964815069281475244480336720618108262665997707387594045170650286331094075335771255196970298123339129317833157961011527832876727076344754954725939644758068479530394261225267979368085014589570504346427
dp = 10223330953817363123811922583649696214606550602104286204220891355717604605964870127334598738896285490397615099445491494493968669242516576783690807635432479

c = 153408111238083132625075217386160278201089187923862024676103784080001237826514301713735771160917544373591779610748265147756784683926730761236534493663419614238905006609729514145435055984994364128927411759418067871721495104602569203564450508769250852903921152143258615277062069536567367247248160384585690407058

for m in range(1, e):
    if (e * dp - 1) % m != 0:
        continue
    p = 1 + (e * dp - 1) // m
    if n % p != 0:
        continue
    q = n // p
    d = modinv(e, (p-1)*(q-1))
    flag = pow(c, d, n)
    print(unhexlify(hex(flag)[2:])) # use [2:-1] for python 2

```

The script's output is `flag{wow_leaking_dp_breaks_rsa?_47689841281}`
