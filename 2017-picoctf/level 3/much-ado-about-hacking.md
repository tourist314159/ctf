[Go back](./Readme.md)

### Much Ado About Hacking - 165 points

The given text is [Shakespeare](https://en.wikipedia.org/wiki/Shakespeare_Programming_Language) code. After manually "compiling it" to a shorter pseudocode version I saw that it reads a string until it encounters a space character and then starts printing it in reverse order, applying a transformation to each character based on the ones it has already printed. I first tried reversing this transformation but failed, so I wrote this script that bruteforces the task. I couldn't get the spl file to compile to c so I used [Lingua::Shakespeare](http://search.cpan.org/dist/Lingua-Shakespeare/lib/Lingua/Shakespeare.pod) and added `use Lingua::Shakespeare;` at the beginning of the downloaded code so that I can tun it like a perl script: `perl MuchAdoAboutHacking.spl`.

```py
from subprocess import Popen, PIPE

target = r"tu1|\h+&g\OP7@% :BH7M6m3g="
flag = ""

valid_chars = [chr(i) for i in range(33, 127)]

while len(flag) < len(target):
	for c in valid_chars:
		guess = c + flag + " "
		p = Popen(["/usr/bin/perl", "MuchAdoAboutHacking.spl"], stdin=PIPE, stdout=PIPE)
		r = p.communicate(guess.encode("ascii"))[0].decode()
		if target.startswith(r):
			flag = c + flag
			print("-> " + flag)
			break
```

Output:

```
$ python ado.py
-> t
-> !t
-> 0!t
-> l0!t
-> pl0!t
-> xpl0!t
-> 3xpl0!t
-> s3xpl0!t
-> ts3xpl0!t
-> hts3xpl0!t
-> ghts3xpl0!t
-> ights3xpl0!t
-> Nights3xpl0!t
-> rNights3xpl0!t
-> 3rNights3xpl0!t
-> m3rNights3xpl0!t
-> Mm3rNights3xpl0!t
-> uMm3rNights3xpl0!t
-> SuMm3rNights3xpl0!t
-> dSuMm3rNights3xpl0!t
-> idSuMm3rNights3xpl0!t
-> MidSuMm3rNights3xpl0!t
-> @MidSuMm3rNights3xpl0!t
-> s@MidSuMm3rNights3xpl0!t
-> ts@MidSuMm3rNights3xpl0!t
-> Its@MidSuMm3rNights3xpl0!t
```
