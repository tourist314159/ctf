[Go back](./Readme.md)

### Biscuit - 75 points

The webpage contains this comment:

```html
<!-- Storing stuff in the same directory as your web server doesn't seem like a good idea -->
<!-- Thankfully, we use a hidden one that is super PRIVATE, to protect our cookies.sqlite file -->
```

I downloaded /private/cookies.sqlite and opened it in SQLite Studio. The database contains one table `moz_cookies` with one entry resembling an HTTP cookie with name "ID" and value "F3MAqpWxIvESiUNLHsflVd". Setting that cookie in the browser console with `document.cookie = "ID=F3MAqpWxIvESiUNLHsflVd"` and refreshing the page shows this message:

```
our flag is: e5cd648b3f7e254fa0c384e6757233f7
```
