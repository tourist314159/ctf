[Go back](./Readme.md)

### Weird RSA - 90 points

The given values are used to optimise RSA using the Chinese Remainder Theorem, as described [on wikipedia](https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Using_the_Chinese_remainder_algorithm).

```py
q_inv = modinv(q, p)
m1 = pow(c, dp, p)
m2 = pow(c, dq, q)
h = q_inv * (m1 - m2) % p
m = m2 + h * q
print(unhexlify(hex(m)[2:-1])) # => 'Theres_more_than_one_way_to_RSA'
```
