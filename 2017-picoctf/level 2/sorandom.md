[Go back](./Readme.md)

### SoRandom - 75 points

As the script running on the server uses the same seed (the string "random") for the PRNG that's used in a sort of a variable Caesar Cipher, I can just replay the cipher and shift in the opposite direction:

```py
import random
encrypted = "BNZQ:jn0y1313td7975784y0361tp3xou1g44"

random.seed("random")
decrypted = ""
for c in encrypted:
    if c.islower():
        decrypted += chr((ord(c) - ord('a') - random.randrange(0, 26)) % 26 + ord('a'))
    elif c.isupper():
        decrypted += chr((ord(c) - ord('A') - random.randrange(0, 26)) % 26 + ord('A'))
    elif c.isdigit():
        decrypted += chr((ord(c) - ord('0') - random.randrange(0, 10)) % 10 + ord('0'))
    else:
        decrypted += c
return decrypted

print(decrypted)
```

(note that this only worked on a linux vps. Both the windows version of python and when I ran it under WSL gave bad answers. YMMV)
