[Go back](./Readme.md)

### Just Keyp Trying - 80 points

I had to skim the [provided pdf](http://www.usb.org/developers/hidpage/Hut1_12v2.pdf) a few times before figuring this one out. Wireshark shows some interesting leftover data in half of the packets:

![Packet data with the interesting bytes highlighted](./just-keyp-trying.png)

The middle bytes correspond to the hex values in Table 12 (pages 53-59) and I guessed the first byte is 0x20 when the shift key is pressed down. Basically values 0x04 to 0x1d correspond to the english alphabet, 0x1e to 0x27 are the digits 1, 2, ..., 9, 0, 0x2d is the hyphen or underscore, 0x2f is [ or { and 0x30 ] or }. Using that table I decoded the flag: `flag{pr355_0nwards_3a10134e}`
