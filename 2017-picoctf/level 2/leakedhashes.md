[Go back](./Readme.md)

### LeakedHashes - 90 points

I used [this](https://crackstation.net/) to look up the first 20 hashes and logged in with one of the "cracked" passwords:

```
$ nc shell2017.picoctf.com 4534
enter username:
christene
christene's password:p0lyp
welcome to shady file server. would you like to access the cat ascii art database? y/n
y

some cat ASCII art...

     /\__/\
    /`    '\
  === 0  0 ===
    \  --  /    - flag is 80b6d6e7a54b0ea26c176fedc700ed43

   /        \
  /          \
 |            |
  \  ||  ||  /
   \_oo__oo_/#######o

... more cats
```
