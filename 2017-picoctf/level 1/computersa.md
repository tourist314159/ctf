[Go back](./Readme.md)

### computeRSA - 50 points

The task requires decrypting the RSA message c=150815 with d=1941 and N=435979. The result value is c^d mod N. This can be easily calculated with python's `pow` function:

```
>>> pow(150815, 1949, 435979)
133337
```
