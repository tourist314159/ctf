[Go back](./Readme.md)

### Leaf of the Tree - 20 points

`find` can be used to see the directory structure of the weird folder.

```
trupples@shell-web:~$ cd /problems/b70fe815d84b75004f724241458ea9cc
trupples@shell-web:/problems/b70fe815d84b75004f724241458ea9cc$ find .
.
./trunk
./trunk/trunk575b
./trunk/trunk575b/trunk5181
./trunk/trunk575b/trunk5181/trunk9560
./trunk/trunk575b/trunk5181/trunk9560/trunkd252
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/branch80ee
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/branch80ee/leaf6339
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768/trunke25a
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768/trunke25a/trunk92ac
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768/trunke25a/trunk92ac/flag
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768/trunke25a/trunk92ac/branchb2d3
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768/trunke25a/trunk92ac/branchb2d3/leafa4ba
./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768/trunke25a/trunk92ac/branchb2d3/leaffbbe
./trunk/trunk575b/trunk5181/branch78c1
./trunk/trunk575b/trunk5181/branch78c1/leaf71d5
./trunk/trunk575b/trunk5181/branch78c1/leafbc53
./trunk/trunk575b/trunk5181/branch78c1/leafa6db
trupples@shell-web:/problems/b70fe815d84b75004f724241458ea9cc$ cat ./trunk/trunk575b/trunk5181/trunk9560/trunkd252/trunk8768/trunke25a/trunk92ac/flag
dbda9f51c858f5ed97b80a9c5a536015
```
