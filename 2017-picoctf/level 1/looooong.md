[Go back](./Readme.md)

### looooong - 20 points

This task generates challenges like this:

```
$ nc shell2017.picoctf.com 22531
To prove your skills, you must pass this test.
Please give me the '<random character>' character '<random 2-3 digit number>' times, followed by a single '<random character>'.
To make things interesting, you have 30 seconds.
```

In another window I opened up python and wrote this simple function:

```
>>> def solve(char, count, end):
...     print(char * count + end)
```

And used it to generate the long string and get the flag:

```
To prove your skills, you must pass this test.
Please give me the 'Z' character '731' times, followed by a single '7'.
To make things interesting, you have 30 seconds.
Input:
ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ7
You got it! You're super quick!
Flag: with_some_recognition_and_training_delusions_become_glimpses_fbafb1011720def036b5aa32671f3710
```
