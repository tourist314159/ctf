[Go back](./Readme.md)

## Master challenge - Lazy Dev - 50 points

In `client.js` the function `validate(password)` always returns false. Overwriting that function with one that always returns true and logging in  outputs the flag:

![screenshot with the flag client_side_is_the_dark_sidee5dbd5f8c6ae5e282766571e06569d50](master.png)
