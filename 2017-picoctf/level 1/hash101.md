[Go back](./Readme.md)

### Hash101 - 50 points

#### Levels 1 and 2

Duckduckgo instant answers to the resque! Searching for `0b0111000001100101011000010110001101100101 to ascii`, hex and decimal quickly gave me the 3 required answers.

```
$ nc shell2017.picoctf.com 9661
Welcome to Hashes 101!
...
TO UNLOCK NEXT LEVEL, give me the ASCII representation of 0111000001100101011000010110001101100101

>peace
Correct! Completed level 1
...
TO UNLOCK NEXT LEVEL, give me the text you just decoded, peace, as its hex equivalent, and then the decimal equivalent of that hex number ("foo" -> 666f6f -> 6713199)

hex>7065616365
Good job! 7065616365 to ASCII -> peace is peace
Now decimal
dec>482737218405
Good job! 482737218405 to Hex -> 7065616365 to ASCII -> peace is peace
...
```

#### Level 3

The ASCII value of '2' is 2 modulo 16. Not much more to this other than that

```
...For example, a simple Hashing Function could be to add up the sum of all the values of all the bytes in the data and get the remainder after dividing by 16 (modulus 16)
TO UNLOCK NEXT LEVEL, give me a string that will result in a 2 after being transformed with the mentioned example hashing function

>2
Correct! Completed level 3
```

#### Level 4

One of the easiest approaches to reversing md5 hashes is a rainbow table lookup. I used [the first one I found](https://crackstation.net/).

```
...
TO CLAIM YOUR PRIZE, give me the string password that will result in this MD5 hash (MD5, like most hashes, are represented as hex digits):
89689941d40794e311ef8bc7061b9944

>7h1ck
Correct! Completed level 4
You completed all 4 levels! Here is your prize: c3ee093f26ba147ccc451fd13c91ffce
```
