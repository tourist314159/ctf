import sys
import h5py

assert(len(sys.argv) == 2)

f = h5py.File(sys.argv[1], "r")

#print(dir(f))

attrs = f.attrs

print("Attrs:")
for kv in attrs.items():
	print("\t{} = {}".format(kv[0], kv[1]))

#print("Attrs:", list(f.attrs.items()))
#print("Items:", f.items)
#print("Keys:", f.keys)
